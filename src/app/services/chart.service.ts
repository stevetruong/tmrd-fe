import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { DEFAULT_PAGE, DEFAULT_SIZE } from '../helper/pagination';

@Injectable({
  providedIn: 'root',
})
export class ChartService {
  constructor(private _http: HttpClient) {}

  getTableData(page?: number, size?: number): Observable<any> {
    page = page ?? DEFAULT_PAGE;
    size = size ?? DEFAULT_SIZE;

    return this._http.get(
      `http://localhost:3000/emergency/data/?page=${page}&size=${size}`
    );
  }

  getAttendantData(trust: string): Observable<any> {
    return this._http.get(
      `http://localhost:3000/emergency/total-attend/?trust=${trust}`
    );
  }
}
