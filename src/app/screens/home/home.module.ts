import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgChartsModule } from 'ng2-charts';

import { HomeRoutingModule } from './home-routing.module';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module

import { HomeComponent } from './home.component';

import { HeaderModule } from 'src/app/components/header/header.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { ChartService } from 'src/app/services/chart.service';

const IMPORT_MODULES = [
  CommonModule,
  HomeRoutingModule,
  HeaderModule,
  FooterModule,
  NgChartsModule,
  NgxPaginationModule,
];

@NgModule({
  declarations: [HomeComponent],
  imports: [...IMPORT_MODULES],
  exports: [HomeComponent],
  providers: [ChartService],
})
export class HomeModule {}
